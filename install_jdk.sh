
JAVA_HOME=/usr/lib/jvm/java-1.7-oracle
JRE_HOME=${JAVA_HOME}/jre
NGINX_URL=10.251.147.144:8080
JDK_VERSION=1.7.0_79
#mkdir /usr/local/java
#cd /usr/local/java
mkdir /home/download
cd /home/download
#download jdk 1.7.67

#wget -O  /home/download/ http://${NGINX_URL}/jenkins/jdk-7-linux-i586.tar.gz
#curl -s -O /home/download/ http://${NGINX_URL}/jenkins/jdk-7u79-linux-x64.tar.gz


curl --silent --location --retry 3 \
http://${NGINX_URL}/jenkins/jdk-7u79-linux-x64.tar.gz \
	| tar xz -C /tmp && \
	mkdir -p /usr/lib/jvm && cp -r /tmp/jdk1.7.0_79/. "${JAVA_HOME}" 



#extract jdk
#tar -zxvf /home/download/jdk-7u79-linux-x64.tar.gz  && \
#mv /home/download/jdk-7u79

#set environment
#export JAVA_HOME="${JAVA_HOME}"
#if ! grep "JAVA_HOME=${JAVA_HOME}" /etc/environment 
#then
#  echo "JAVA_HOME=${JAVA_HOME}" | sudo tee -a /etc/environment 
#  echo "export JAVA_HOME" | sudo tee -a /etc/environment 
#  echo "PATH=$PATH:$JAVA_HOME/bin" | sudo tee -a /etc/environment 
#  echo "export PATH" | sudo tee -a /etc/environment 
#  echo "CLASSPATH=.:$JAVA_HOME/lib" | sudo tee -a /etc/environment 
#  echo "export CLASSPATH" | sudo tee -a /etc/environment 
#fi
	update-alternatives --install "/usr/bin/java" "java" "${JRE_HOME}/bin/java" 1 && \
	update-alternatives --install "/usr/bin/javac" "javac" "${JAVA_HOME}/bin/javac" 1 && \
	update-alternatives --set java "${JRE_HOME}/bin/java" && \
	update-alternatives --set javac "${JAVA_HOME}/bin/javac"


#update environment
#source /etc/environment  
echo "jdk is installed !"
